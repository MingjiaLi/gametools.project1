﻿using System.Diagnostics;
using System.Text.Json;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;

namespace HudEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HudData _hudData;

        private TextBox _curScoreTxt;
        private TextBox _curHealthTxt;
        private TextBox _highestScoreTxt;
        private TextBox _lifeTexture;

        public MainWindow()
        {
            InitializeComponent();
            InitCanvasItems();
        }

        private void InitCanvasItems()
        {
            _curScoreTxt = new TextBox();
            _curScoreTxt.Width = 150;
            _curScoreTxt.Height = 50;
            _curScoreTxt.Text = "Current Score Text";

            _curHealthTxt = new TextBox();
            _curHealthTxt.Width = 150;
            _curHealthTxt.Height = 50;
            _curHealthTxt.Text = "Current Health Text";

            _highestScoreTxt = new TextBox();
            _highestScoreTxt.Width = 150;
            _highestScoreTxt.Height = 50;
            _highestScoreTxt.Text = "Highest Score Text";

            _lifeTexture = new TextBox();
            _lifeTexture.Width = 25;
            _lifeTexture.Height = 25;
            _lifeTexture.Text = "Life";

            RenderCanvas.Children.Add(_curScoreTxt);
            RenderCanvas.Children.Add(_curHealthTxt);
            RenderCanvas.Children.Add(_highestScoreTxt);
            RenderCanvas.Children.Add(_lifeTexture);
        }

        private void OpenFileBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var filePath = FileManager.Instance.LoadFile();

            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            _hudData = JsonManager.Instance.Deserialize<HudData>();
            _hudData.HudSetting.WindowXScale = (float)(RenderCanvas.RenderSize.Width / _hudData.HudSetting.WindowWidth);
            _hudData.HudSetting.WindowYScale = (float)(RenderCanvas.RenderSize.Height / _hudData.HudSetting.WindowHeight);
            this.HUDSettingBox.DataContext = _hudData.HudSetting;
            this.CurScoreBox.DataContext = _hudData.HudSetting.Objects.CurScoreTxt;
            this.CurHealthBox.DataContext = _hudData.HudSetting.Objects.CurHealthTxt;
            this.HighestScoreBox.DataContext = _hudData.HudSetting.Objects.HighestScoreTxt;
            this.LifeTextureBox.DataContext = _hudData.HudSetting.Objects.LifeTexture;
        }

        private void SaveFileBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var jsonStr = JsonManager.Instance.Serialize(_hudData);
            FileManager.Instance.SaveFile(jsonStr);
        }

        private void CurScoreBox_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _curScoreTxt.SetValue(Canvas.LeftProperty, (double)_hudData.HudSetting.Objects.CurScoreTxt.PosX);
            _curScoreTxt.SetValue(Canvas.TopProperty, (double)_hudData.HudSetting.Objects.CurScoreTxt.PosY);
        }

        private void CurHealthBox_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _curHealthTxt.SetValue(Canvas.LeftProperty, (double)_hudData.HudSetting.Objects.CurHealthTxt.PosX);
            _curHealthTxt.SetValue(Canvas.TopProperty, (double)_hudData.HudSetting.Objects.CurHealthTxt.PosY);
        }

        private void HighestScoreBox_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _highestScoreTxt.SetValue(Canvas.LeftProperty, (double)_hudData.HudSetting.Objects.HighestScoreTxt.PosX);
            _highestScoreTxt.SetValue(Canvas.TopProperty, (double)_hudData.HudSetting.Objects.HighestScoreTxt.PosY);
        }

        private void LifeTextureBox_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _lifeTexture.SetValue(Canvas.LeftProperty, (double)_hudData.HudSetting.Objects.LifeTexture.PosX);
            _lifeTexture.SetValue(Canvas.TopProperty, (double)_hudData.HudSetting.Objects.LifeTexture.PosY);
        }

        private void ApplyBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var jsonStr = JsonManager.Instance.Serialize(_hudData);
            _hudData = JsonSerializer.Deserialize<HudData>(jsonStr);
            _hudData.HudSetting.WindowXScale = (float)(RenderCanvas.RenderSize.Width / _hudData.HudSetting.WindowWidth);
            _hudData.HudSetting.WindowYScale = (float)(RenderCanvas.RenderSize.Height / _hudData.HudSetting.WindowHeight);
            this.HUDSettingBox.DataContext = _hudData.HudSetting;
            this.CurScoreBox.DataContext = _hudData.HudSetting.Objects.CurScoreTxt;
            this.CurHealthBox.DataContext = _hudData.HudSetting.Objects.CurHealthTxt;
            this.HighestScoreBox.DataContext = _hudData.HudSetting.Objects.HighestScoreTxt;
            this.LifeTextureBox.DataContext = _hudData.HudSetting.Objects.LifeTexture;
        }
    }
}