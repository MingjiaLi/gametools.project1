﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HudEditor
{
    public class CurScoreTxt
    {
        public float PosX { get; set; }
        public float PosY { get; set; }
        public string Font { get; set; }
        public int CharacterSize { get; set; }
        public string Texture { get; set; }
    }

    public class CurHealthTxt
    {
        public float PosX { get; set; }
        public float PosY { get; set; }
        public string Font { get; set; }
        public int CharacterSize { get; set; }
        public string Texture { get; set; }
    }

    public class HighestScoreTxt
    {
        public float PosX { get; set; }
        public float PosY { get; set; }
        public string Font { get; set; }
        public int CharacterSize { get; set; }
        public string Texture { get; set; }
    }

    public class LifeTexture
    {
        public float PosX { get; set; }
        public float PosY { get; set; }
        public string Font { get; set; }
        public int CharacterSize { get; set; }
        public string Texture { get; set; }
    }

    public class Objects
    {
        public CurScoreTxt CurScoreTxt { get; set; }
        public CurHealthTxt CurHealthTxt { get; set; }
        public HighestScoreTxt HighestScoreTxt { get; set; }
        public LifeTexture LifeTexture { get; set; }
    }

    public class HudSetting
    {
        public int WindowWidth { get; set; }
        public int WindowHeight { get; set; }
        public float WindowXScale { get; set; }
        public float WindowYScale { get; set; }
        public Objects Objects { get; set; }
    }

    public class HudData
    {
        public HudSetting HudSetting { get; set; }
    }
}