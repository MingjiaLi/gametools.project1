﻿using Microsoft.Data.Sqlite;
using System.Collections.Generic;

namespace AnalyzeShipGame
{
    internal class SQLiteManager
    {
        private static SQLiteManager _instance;

        public static SQLiteManager Instance
        {
            get
            {
                _instance ??= new SQLiteManager();
                return _instance;
            }
        }

        public string DBPath
        {
            get;
            set;
        }

        public List<RoundData> GetAllData()
        {
            using (var sqliteConnection = new SqliteConnection($"Data Source = {DBPath}"))
            {
                sqliteConnection.Open();

                var sqlCommand = sqliteConnection.CreateCommand();
                var tableName = "ShipGame";
                var command = $"SELECT * FROM {tableName};";
                sqlCommand.CommandText = command;
                var roundDataList = new List<RoundData>();

                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var roundData = new RoundData
                        {
                            PlayerDiedCount = reader["playerDiedCount"].ToString(),
                            DestroyedShipCount = reader["destroyedShipCount"].ToString(),
                            DestroyedAsteroidCount = reader["destroyedAsteroidCount"].ToString(),
                            TotalAsteroidCount = reader["totalAsteroidCount"].ToString(),
                            TotalShipCount = reader["totalShipCount"].ToString(),
                        };
                        roundDataList.Add(roundData);
                    }
                }

                sqliteConnection.Close();

                return roundDataList;
            }
        }
    }

    internal class RoundData
    {
        public string PlayerDiedCount { get; set; }
        public string DestroyedShipCount { get; set; }
        public string DestroyedAsteroidCount { get; set; }
        public string TotalAsteroidCount { get; set; }
        public string TotalShipCount { get; set; }
    }
}