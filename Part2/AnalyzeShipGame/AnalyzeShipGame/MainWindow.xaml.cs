﻿using System.Diagnostics;
using System.Windows;
using Microsoft.Win32;

namespace AnalyzeShipGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenDbBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog()
            {
                Filter = "DB Files (*.db) | *.db"
            };

            var result = openFileDialog.ShowDialog();
            if (result == true)
            {
                SQLiteManager.Instance.DBPath = openFileDialog.FileName;
                ShowData();
            }
        }

        private void ShowData()
        {
            var dataList = SQLiteManager.Instance.GetAllData();
            for (int i = 0; i < dataList.Count; i++)
            {
                this.DataListView.Items.Add(dataList[i]);
            }
        }
    }
}