﻿using System;
using System.IO;
using Microsoft.Win32;

namespace GameTools.Project1
{
    internal class FileManager
    {
        private static FileManager _instance;

        public static FileManager Instance
        {
            get
            {
                _instance ??= new FileManager();
                return _instance;
            }
        }

        private string _openFilePath = string.Empty;

        public string LoadFile()
        {
            _openFilePath = OpenDialog();
            JsonManager.Instance.JsonFileStr = FileToString(_openFilePath);
            return _openFilePath;
        }

        public bool SaveFile(string data)
        {
            var isAppend = string.IsNullOrEmpty(_openFilePath);
            var filePath = _openFilePath;

            // if _openFilePath is empty, save as a new file
            if (isAppend)
            {
                var dialog = new SaveFileDialog
                {
                    Filter = "Json File (*.json) | *.json",
                    RestoreDirectory = true,
                    FilterIndex = 1
                };

                if (dialog.ShowDialog() == true)
                {
                    filePath = dialog.FileName;
                }
            }

            if (string.IsNullOrEmpty(filePath))
            {
                return false;
            }

            using var streamWriter = new StreamWriter(filePath, isAppend);
            streamWriter.Write(data);
            streamWriter.Close();
            streamWriter.Dispose();

            return true;
        }

        private string OpenDialog()
        {
            var openFileDialog = new OpenFileDialog()
            {
                Filter = "JSON Files (*.json) | *.json"
            };

            var result = openFileDialog.ShowDialog();
            if (result == true)
            {
                return openFileDialog.FileName;
            }

            return string.Empty;
        }

        private string FileToString(string filePath)
        {
            var str = "";
            if (File.Exists(filePath))
            {
                using var file = new StreamReader(filePath);
                str = file.ReadToEnd();
                file.Close();
                file.Dispose();
            }
            return str;
        }
    }
}