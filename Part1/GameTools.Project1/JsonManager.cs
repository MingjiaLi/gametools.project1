﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace GameTools.Project1
{
    internal class JsonManager
    {
        private static JsonManager _instance;

        public static JsonManager Instance
        {
            get
            {
                _instance ??= new JsonManager();
                return _instance;
            }
        }

        public string JsonFileStr { get; set; }

        public T Deserialize<T>() where T : new()
        {
            T result = JsonSerializer.Deserialize<T>(JsonFileStr);
            return result;
        }

        public string Serialize<T>(T data) where T : new()
        {
            var jsonStr = JsonSerializer.Serialize(data);
            return jsonStr;
        }
    }
}