﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameTools.Project1
{
    public class GameObjectsItem
    {
        public string className { get; set; }
        public string name { get; set; }
        public string texture { get; set; }
        public int livesCount { get; set; }
        public float speed { get; set; }
        public float damage { get; set; }
        public float score { get; set; }
        public float HP { get; set; }
        public float posX { get; set; }
        public float posY { get; set; }
    }

    internal class GameLevel
    {
        public List<GameObjectsItem> GameObjects { get; set; }
    }
}