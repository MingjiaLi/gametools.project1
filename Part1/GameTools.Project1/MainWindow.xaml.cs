﻿using System;
using System.Diagnostics;
using System.Windows;

namespace GameTools.Project1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GameLevel _gameLevel;
        private GameSetting _gameSetting;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void SaveGameSettingBtn_OnClick(object sender, RoutedEventArgs e)
        {
            if (DataManager.Instance.GameSettingData == null)
            {
                this.StatusLabel.Content = "Game Setting Data is null";
                return;
            }

            var jsonStr = JsonManager.Instance.Serialize(DataManager.Instance.GameSettingData);
            var result = FileManager.Instance.SaveFile(jsonStr);
            var resultStr = result ? "Saved successfully" : "Save failed";
            this.StatusLabel.Content = resultStr;
        }

        private void SaveGameLevelBtn_OnClick(object sender, RoutedEventArgs e)
        {
            if (DataManager.Instance.GameLevelData == null)
            {
                this.StatusLabel.Content = "Game Level Data is null";
                return;
            }

            var jsonStr = JsonManager.Instance.Serialize(DataManager.Instance.GameLevelData);
            var result = FileManager.Instance.SaveFile(jsonStr);
            var resultStr = result ? "Saved successfully" : "Save failed";
            this.StatusLabel.Content = resultStr;
        }

        private void LoadFileBtn_OnClick(object sender, RoutedEventArgs e)
        {
            _gameSetting = null;
            _gameLevel = null;

            var filePath = FileManager.Instance.LoadFile();
            if (string.IsNullOrEmpty(filePath))
            {
                return;
            }

            if (filePath.Contains("GameSetting"))
            {
                _gameSetting = JsonManager.Instance.Deserialize<GameSetting>();
                DataManager.Instance.GameSettingData = _gameSetting;
            }

            if (filePath.Contains("GameLevel"))
            {
                _gameLevel = JsonManager.Instance.Deserialize<GameLevel>();
                DataManager.Instance.GameLevelData = _gameLevel;
            }

            this.StatusLabel.Content = $"Opened File: {filePath}";

            ShowData();
        }

        private void ShowData()
        {
            if (_gameSetting != null)
            {
                this.GameSettingGroup.DataContext = DataManager.Instance.GameSettingData;
                this.AssetsPathList.ItemsSource = DataManager.Instance.GameSettingData.AssetManager.AssetsPath;
            }

            if (_gameLevel != null)
            {
                this.GameLevelList.ItemsSource = DataManager.Instance.GameLevelData.GameObjects;
            }
        }
    }
}