﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameTools.Project1
{
    internal class DataManager
    {
        private static DataManager _instance;

        public static DataManager Instance
        {
            get
            {
                _instance ??= new DataManager();
                return _instance;
            }
        }

        public GameLevel GameLevelData { get; set; }
        public GameSetting GameSettingData { get; set; }
    }
}