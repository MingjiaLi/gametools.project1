﻿using System.Collections.Generic;

namespace GameTools.Project1
{
    public class GameEngine
    {
        public string DefaultFile { get; set; }
    }

    public class RenderSystem
    {
        public string Name { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public bool fullscreen { get; set; }
    }

    public class FileSystem
    {
        public string jsonFilesPath { get; set; }
    }

    public class AssetsPathItem
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }

    public class AssetManager
    {
        public List<AssetsPathItem> AssetsPath { get; set; }
    }

    internal class GameSetting
    {
        public GameEngine GameEngine { get; set; }
        public RenderSystem RenderSystem { get; set; }
        public FileSystem FileSystem { get; set; }
        public AssetManager AssetManager { get; set; }
    }
}